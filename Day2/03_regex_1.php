<?php

$pattern = '/cat/';
$string= "I'm a cat lover";
//'/cat/'--match

//in PHP We use preg_match
//use perl
if(preg_match($pattern, $string)){
    echo '<p>We have a match!</p>';
}
else{
    echo '<p>no match</p>';
}


//preg_match has 3 potential return values;

//0 -- false
//1 -- match
//false -- error

//how do i match exactly 'cat'
$pattern = '/^cat$/';
$pattern2 = '/^cat';
$string1 = 'I am a Cat';
$string2 = 'cat';
var_dump(preg_match($pattern,$string1));
/**/
var_dump(preg_match($pattern, $string2));
/*should return false -- error*/
//var_dump(preg_match($pattern2, $string2));

/*[A-ZA-z] one character either lowercase or uppercase*/
/*[A-z]*/
/*. wild card "to add all characters"*/
/*[A-Za-z0-9^%&*#@]*/
//demonstrate capture groups
/*/^([0-9]{3})-?([0-9]{3})-?([0-9]{4})$/*/
echo"<br/>";
echo"<h1>Matches</h1>";

$pattern3 = '/^([0-9]{3})-?([0-9]{3})-?([0-9]{4})$/';
$num1 = '204-218-5896';
var_dump($pattern3);
echo"<br/>";
var_dump($num1);
echo"<br/>";
var_dump(preg_match($pattern3,$num1,$matches));
echo"<br/>";
var_dump($matches);
echo"<br/>";
$formatted_num = "({$matches[1]}) {$matches[2]} {$matches[3]}";