<?php

//Object Oriented PHP

//javascript
//JS has prototyped inheritance

//PHP
//PHP has classical inheritance
    //inheritance
    //interfaces//abstract classes

//what is class
//class is a blueprint for creating objects
//we can create many objects from the same class

//nomrally, in php each class is stored in its own file,
//with a file name that exaclty matches the class name

//This class would be stored in a file name User.php
//Uppercase CamelCase (PSR-2 style rule)

$user1 = new User();
var_dump($user1);
echo'<br/>';

//When we assign new properties to an object fro outside that object,
// they are by default 'public...' visible everywhere.
$user1->name='Frakie';
$user1->email='juancji00@mail.com';
$user1->age = 12;
//must use public setter method to set the value of private property
//public setters and getters
$user1->setPassword(MD5("Myppas"));
var_dump($user1);
$user1->getPassword();

//properties (And methods) can be...
//public -- visible inside and outside the object.
//protected --visible inside an object, and child object (inheritance)
//private -- visible ONLY Within the object in which they are declared