<?php
require __DIR__. '/functions.php';
require __DIR__. '/config.php';
require __DIR__ . '/../Classes/Validator.php';
$title='Register';
$v=new Validator();
//Test for post
if($_SERVER['REQUEST_METHOD']=='POST')
{
    //validate fields
    $v->required('name');
    $v->required('email');
    $v->required('phone');
    //if no errors
    if(!$v->errors())
    {
        //var_dump SUCCESS
        dd($_POST);
        die;
    }//end if no errors

}//End test for post

?><html>
    <head>
        <title></title><?=esc($title)?></title>

    </head>
    <body>
    <h1><?=esc($title)?></h1>
    $errors = $v->errors();
    <form action="<?=esc_attr($_SERVER['PHP_SELF'])?>" method="post" novalidate>
        <p><label for="name">Name: </label><input type="text" name="name" value="<?=clean('name')?>"></p>
        <p><label for="email">Email: </label><input type="text" name="email" value="<?=clean('email')?>"></p>
        <p><label for="phone">Phone: </label><input type="text" name="phone" value="<?=clean('phone')?>"></p>
    </form>
</body>
</html>
