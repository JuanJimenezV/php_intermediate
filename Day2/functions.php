<?php
require __DIR__ . '/config.php';
/**
 * My Functions File
 * author: Juan Jimenez, jimenezvalenzuela-j@wemail.uwwinnipeg.ca
 * creation date: 08/08/2019
 */

/**
 * Escape string for output to HTML
 * @param  String $string
 * @return String
 */
function esc($string) 
{
    return htmlentities($string, null, "UTF-8");
}

/**
 * Escape string for output to HTML attribute
 * @param  String $string
 * @return String
 */
function esc_attr($string)
{
    return htmlentities($string, ENT_QUOTES, "UTF-8");
}

/**
 * Dump variable using var_dump
 * @param  Mixed $var
 * @return Void
 */
function dd($var)
{
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

/**
 * Return sanitized POST values
 * @param  String $field $_POST field name
 * @return String the sanitized string
 */
function clean($field)
{
    if(!empty($_POST[$field])) {
        return esc_attr($_POST[$field]);
    } else {
        return '';
    }
}

/**Insert info for the
 * @param $dbh
 * @param $FirstName
 * @param $LastName
 * @param $Street
 * @param $City
 * @param $PostalCode
 * @param $Province
 * @param $Country
 * @param $Phone
 * @param $Email
 * @param $Password
 * @return mixed
 */
function InsertInfo($dbh, $FirstName, $LastName, $Street, $City, $PostalCode, $Province, $Country, $Phone, $Email,$Password)
{
    $query = "INSERT INTO users(FirstName,LastName,Street,City,PostalCode,Province,Country,Phone,Email,Password)VALUES
              (:FirstName,:LastName,:Street,:City,:PostalCode,:Province,:Country,:Phone,:Email,:Password)";
    $stmt = $dbh->prepare($query);
    $params=[
        ':FirstName'=>$FirstName,
        ':LastName'=>$LastName,
        ':Street'=>$Street,
        ':City'=>$City,
        ':PostalCode'=>$PostalCode,
        ':Province'=>$Province,
        ':Country'=>$Country,
        ':Phone'=>$Phone,
        ':Email'=>$Email,
        ':Password'=>$Password];
    $stmt->execute($params);
    $user_id = $dbh->lastInsertId();
    return $user_id;
}

/**Function that returns the information of the user
 * @param $dbh
 * @param $user_id
 * @return mixed
 */
function GetUserByID($dbh, $user_id){
    $query = "SELECT * FROM users WHERE USERID = :user_id";
    $stmt = $dbh->prepare($query);
    $params = [':user_id' => (int) $user_id];
    $stmt->execute($params);
    return $stmt->fetch(PDO::FETCH_ASSOC);
}
function Label($string){
    return ucwords(str_replace('_',' ',$string));
}