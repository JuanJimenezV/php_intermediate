<?php
require __DIR__ .'/autoloaders.php';

/*Use Statement*/
use App\Validator;
use App\Auth\User;
use App\Users\User as Person;

//PSR4 standard for namespaces
//respect Capitalization
//file name same as class

$v = new Validator;
$user = new User;

var_dump($v);
var_dump($user);
