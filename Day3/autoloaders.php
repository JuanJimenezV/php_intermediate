<?php


//spl_autoload_register(function(){
//code to locate and require the class file
//});

/**
 * @param String
 */
spl_autoload_register
(
    function ($class)
    {
        $prefix = "App\\";
        //Base directory where my class reside
        $base_dir = __DIR__ . '/juan/';
        // get the length if the prefix
        $len = strlen($prefix);
        //Test That the class name passed in is using the prefix
        if(strncmp($prefix, $class,$len)!==0)
        {
            return;
        }

        $sub_class = substr($class, $len);
        $file = $base_dir . str_replace('\\',DIRECTORY_SEPARATOR,$sub_class) . '.php';
        if(file_exists($file))
        {
            require $file;
        }
    }
);


