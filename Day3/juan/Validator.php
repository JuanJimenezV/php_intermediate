<?php
namespace App;

class Validator
{

    /**Array for tracking validation errors
     * @var array
     */
    protected $errors = [];
    protected $post;

    public function __construct()
    {
        if($post= filter_input_array(INPUT_POST))
        {
            foreach($_POST as $key => $value)
            {
                $this->post[$key] = trim($value);
            }
        }
    }

    public function required($field){
        if(empty($this->post[$field]))
        {
            $label=$this->label($field);

        }
    }

    //Create method to validate phone number, It should accept the field name
    public function Phone($field)
    {
        $pattern = '/^([0-9]{3})-?([0-9]{3})-?([0-9]{4})$/';
        if(preg_match($pattern, $this->post[$field])!==1)
        {
            $this->setErrors($field, 'Please enter a valid number');
        }
    }

    /**
     * @return Array
     */
    public function errors(){
        return $this->errors;
    }
    protected function setErrors($field, $message)
    {
        if(empty($this->errors[$field]))
        {
            $this->errors[$field] = $message;
        }
    }
    protected function label($string){
        return ucwords(str_replace('_',' ',$string));
    }
}