<?php

require  __DIR__ .'/../autoloaders.php';

use PHPUnit\Framework\TestCase;
use App\Input;
final class InputTest extends TestCase
{
    public function testInputCanBeInstantiated()
    {
        $i= new Input();
        $this->assertInstanceOf(Validator::class,$i);
    }

    public function testInputReturnsRawPostArray()
    {
        $_POST = array(
            'title'=>'<h1>Our Title</h1>',
            'content' => '<p>lorem impsum</p>'
        );

        $i = new Input();
        $post = $i->post();
        $this->asserCount(2,$post);
    }

    public function testInputRetunedRawArrayContainsTags()
    {
        $_POST = array(
            'title'=>'<h1>Our Title</h1>',
            'content' => '<p>lorem impsum</p>'
        );
        $i = new Input();
        $post = $i->post();
        $actual = $post['title'];
        $expected = '<h1>Our Title</h1>';
        $this->assertEquals($actual, $expected);
    }

    public function testInputRetunsSanitaziedArray()
    {
        $_POST = array(
            'title'=>'<h1>Our Title</h1>',
            'content' => '<p>lorem impsum</p>'
        );
        $i = new Input();
        $post = $i->cleanPost();
        $this->asserCount(2,$post);
    }

    public function testInputRetunsCleanArrayContansEntities()
    {
        $_POST = array(
            'title'=>'<h1>Our Title</h1>',
            'content' => '<p>lorem impsum</p>'
        );
        $i = new Input();
        $post = $i->cleanPost();
        $this->asserCount(2,$post);
    }

    public function testInputReturnsSingleFieldOnRequest()
    {
        $_POST = array(
            'title'=>'<h1>Our Title</h1>',
            'content' => '<p>lorem impsum</p>'
        );

        $i = new Input();
        $actual =$i->post('title');
        $expected = '<h1>Our Title</h1>';
        $this->assertEquals($actual, $expected);
    }
}