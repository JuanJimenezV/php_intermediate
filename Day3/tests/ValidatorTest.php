<?php

require  __DIR__ .'/../autoloaders.php';

use PHPUnit\Framework\TestCase;
use App\Validator;
final class ValidatorTest extends TestCase
{
    public function testValidatorCanBeInstantiated()
    {
        $v= new Validator();
        $this->assertInstanceOf(Validator::class,$v);
    }
    public function testRequiredMethodReturnsErrorOnEmptyField()
    {
        $_POST=['phone'];
        $v = new Validator();
        $v->required('phone=> ');
        $this->assertArrayHasKey('phone',$v->errors());
    }
    public function testRequiredMethodReturnsNoErrorsValueExists()
    {
        $_POST=['phone'=>'123-123-3456'];
        $v = new Validator();
        $v->required('phone');
        $this->assertEmpty($v->errors());
    }
    public function testRequiredMethodReturnsErrorOnFieldWithSpace()
    {
        $_POST=['phone'=>' '];
        $v = new Validator();
        $v->required('phone');
        $this->assertArrayHasKey('phone',$v->errors());
    }
    //test that valid phone returns no errors
    public function testRequiredMethodReturnsNoErrorsOnValidPhone()
    {
        $_POST=['phone'=>'204-218-2890'];
        $v = new Validator();
        $v->required('phone=>204-218-2890');
        $this->assertArrayHasKey('phone',$v->errors());
    }
}