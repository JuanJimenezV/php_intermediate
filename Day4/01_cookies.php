<?php
//if the cookie 'test' is not already set, we will go ahead and set it.
if(empty($_COOKIE['test']))
{
    echo '<p>about to set a cookie</p>';
    //command to set a cookie:setcookie
    setcookie('test','I am a cookie',time()+60*60*24*30);
}
//all cookie data is accessible in the superglobal $_COOKIE
//cookie data should be considered tainted
//our cookie data will not visible in the $_COOKIE
//array until the next request
var_dump($_COOKIE);
?><!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
    <h1>Cookie test</h1>
    <h2>I am inside a div</h2>
    <p><a href="02_cookies_cont.php">Page 2</a></p>
</body>
<html>