<?php
if(empty($_COOKIE['test']))
{
    $test=$_COOKIE['test'];
}
else
{
    $test = 'Cookie was not set';
}
if(!empty($_GET['delete_cookie']))
{
    setcookie('test',null, -3600);
    header('Location: 01_cookies.php');
    die;
}
?><!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <h1><?=htmlentities($test,null,'UTF-8')?></h1>
        <div class="<?=htmlentities($test, ENT_QUOTES,"UTF-8")?>">
            <h2>I am inside a div!</h2>
            <p><a href="01_cookies.php">Back</a></p>
            <p><a href="?delete_cookie=1">Delete Cookie</a></p>
        </div>
    </body>
<html>

