<?php
session_start();
// set error reporting
ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

// Define DB connect constants
define('DB_DSN', 'mysql:host=localhost:3307;dbname=users');
define('DB_USER', 'web_user');
define('DB_PASS', 'mypass');

// 1. Create DB connection
$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
// set the $dbh to display or print errors into the dbh variable if there are any
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

