<?php
    require __DIR__ . '/../config.php';

    $errors = [];
    if(!empty($_SESSION['logout']))
    {
        session_regenerate_id();
        session_destroy();
        $_SESSION['flash'] ='you have been successfully logged out.';
        header('Location:login.php');
        die;
    }

    if($_SERVER['REQUEST_METHOD'] =='POST')
    {
        if(empty($_POST['email']) || empty($_POST['password'])){
            $errors[] = 'Both email and password are required';
        }
        if(count($errors)==0)
        {
            $query = 'SELECT * FROM USERS where email=:email';
            $params = array(':email'=>$_POST['email']);
            $stmt =$dbh->prepare($query);
            $stmt->execute($params);
            $usr = $stmt->fetch(PDO::FETCH_ASSOC);
            
            if($usr==null)
            {
                exit('no such user here');
            }
            if (password_verify($_POST['password'],$usr['password']))
            {
                $_SESSION['logged_in'] = true;
                $_SESSION['user_id'] = $usr['id'];
                session_regenerate_id(true);
                header('Location: profile.php');
                die;
            }
            else
            {
                unset($_SESSION['Logged_in']);
                $errors[]='Login credentials do not match';
            }
            var_dump($_SESSION['Logged_in']);
        }
    }//end of POST

?><!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="UTF-8">
    <title>home</title>
</head>
<body>
<h1>Login</h1>
<?php require 'nav.inc.php'?>
<form action="<?=$_SERVER['PHP_SELF']?>" method ="post">
    <p>
        <label for="email">Email: </label>
        <input type="text" name ="email">
    </p>
    <p>
        <label for="password">Password: </label>
        <input type="text" name ="password">
    </p>
    <p>
        <button type="submit">login</button>
    </p>
</form>
</body>
<html>