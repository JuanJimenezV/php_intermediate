<?php
    require __DIR__ . '/../config.php';

    $errors = [];


    if($_SERVER['REQUEST_METHOD'] =='POST')
    {
        //ultra simple validation
        if(empty($_POST['email']) || empty($_POST['password'])){
            $errors[] = 'Both email and password are required';
        }
        if(count($errors)==0){
            $query = "INSERT INTO users(email,password)VALUES(:email, :password)";
            $params = array(':email' => $_POST['email'],
                ':password'=>password_hash($_POST['password'],PASSWORD_DEFAULT));
            //prepare
            $stmt = $dbh->prepare($query);
            // execute
            $stmt -> execute($params);
            $user_id = $dbh->lastInsertId();
            //get last id
            if($user_id!=0)
            {
                $_SESSION['id']=$user_id;
                $_SESSION['Logged_in']=true;
                session_regenerate_id(true);
                $SESSION['flash']='You have been successfully registered';
                header('Location:login.php');
                die;
            }//end if
        }
    }//end of POST

?><!DOCTYPE html>
<html lang="eng">
<head>
    <meta charset="UTF-8">
    <title>Register</title>
</head>
<body>
<h1>Register</h1>
<?php require 'nav.inc.php'?>
<form action="<?=$_SERVER['PHP_SELF']?>" method ="post">
    <p>
        <label for="email">Email: </label>
        <input type="text" name ="email">
    </p>
    <p>
        <label for="password">Password: </label>
        <input type="text" name ="password">
    </p>
    <p>
        <button type="submit">login</button>
    </p>
</form>
</body>
<html>